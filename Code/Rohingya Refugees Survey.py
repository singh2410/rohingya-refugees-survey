#!/usr/bin/env python
# coding: utf-8

# # Rohingya Refugee Analysis
# #By- Aarush Kumar
# #Dated:November 04,2021

# In[1]:


from IPython.display import Image
Image(url='https://images.squarespace-cdn.com/content/v1/57052f155559869b68a4f0e6/1534355663600-OOGZ9OY9SYS44X73JPHN/ke17ZwdGBToddI8pDm48kJanlAjKydPZDDRBEy8QTGN7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0hveExjbswnAj1UrRPScjfAi-WHBb3R4axoAEB7lfybbrcBqLQ3Qt4YGS4XJxXD2Ag/COVER+-+RFI-Rohingya+Crisis+-+August+2018.jpg?format=500w')


# In[2]:


import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt
import plotly.express as px
import seaborn as sns
import plotly.graph_objects as go
import plotly.offline as py


# In[3]:


nRowsRead = 1000 # specify 'None' if want to read whole file
df = pd.read_csv('/home/aarush100616/Downloads/Projects/Rohingya Survey/Dataset/rohingya.csv', delimiter=';', encoding = "ISO-8859-1", nrows = nRowsRead)
df.dataframeName = 'rohingya.csv'
nRow, nCol = df.shape
print(f'There are {nRow} rows and {nCol} columns')
df.head()


# In[4]:


df["Sex"].value_counts()


# In[5]:


sns.countplot(x="Sex",data=df,palette="GnBu_d",edgecolor="black")
plt.title('Gender Distribution', weight='bold')
plt.xticks(rotation=45)
plt.yticks(rotation=45)
# changing the font size
sns.set(font_scale=1)


# In[6]:


df["If you could choose any country in the world, other than Myanmar, where would you go?"].value_counts()


# In[7]:


sns.countplot(x="How do you feel about the future?",data=df,palette="rainbow",edgecolor="black")
plt.title('Expectations about the Future', weight='bold')
plt.xticks(rotation=45)
plt.yticks(rotation=45)
# changing the font size
sns.set(font_scale=1)


# In[8]:


df["How satisfied are you with the amount of space allocated to you and your family in the camp?"].value_counts()


# In[9]:


sns.countplot(x="How satisfied are you with the amount of space allocated to you and your family in the camp?",data=df,palette="coolwarm",edgecolor="black")
plt.title('Satisfaction with amount of Space in the Camp', weight='bold')
plt.xticks(rotation=45)
plt.yticks(rotation=45)
# changing the font size
sns.set(font_scale=1)


# In[11]:


fig = px.bar(df[['Sex','How scared are you to leave for another country?']].sort_values('How scared are you to leave for another country?', ascending=False), 
                        y = "How scared are you to leave for another country?", x= "Sex", color='How scared are you to leave for another country?', template='ggplot2')
fig.update_xaxes(tickangle=45, tickfont=dict(family='Rockwell', color='crimson', size=14))
fig.update_layout(title_text="How scared are you to leave for another country?")
fig.show()


# In[12]:


fig = px.bar(df[['Sex','Do you think you are able to exercise your religion freely in the camp?']].sort_values('Do you think you are able to exercise your religion freely in the camp?', ascending=False), 
                        y = "Do you think you are able to exercise your religion freely in the camp?", x= "Sex", color='Do you think you are able to exercise your religion freely in the camp?', template='ggplot2')
fig.update_xaxes(tickangle=45, tickfont=dict(family='Rockwell', color='crimson', size=8))
fig.update_layout(title_text="Do you think you are able to exercise your religion freely in the camp?")
fig.show()


# In[13]:


fig = px.pie(df, values=df['Row_ID'], names=df['Sex'],
             title='Rohingya Refugees by Gender',
            )
fig.update_traces(textposition='inside', textinfo='percent+label')
fig.show()


# In[14]:


import plotly.offline as pyo
import plotly.graph_objs as go
lowerdf = df.groupby('How satisfied are you with the quality of healthcare you and your family are receiving in the camp?').size()/df['Sex'].count()*100
labels = lowerdf.index
values = lowerdf.values

# Use `hole` to create a donut-like pie chart
fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.6)])
fig.show()


# In[15]:


import plotly.express as px

# Grouping it by sex and question
plot_data = df.groupby(['Sex', 'How do you mostly hear about the news?'], as_index=False).Row_ID.sum()

fig = px.bar(plot_data, x='Sex', y='Row_ID', color='How do you mostly hear about the news?', title='How do you mostly hear about the news?')
fig.show()


# In[16]:


import plotly.express as px
# Grouping it by sex and question
plot_data = df.groupby(['Sex', 'Who do you consider the most important person in your camp?'], as_index=False).Row_ID.sum()
fig = px.bar(plot_data, x='Sex', y='Row_ID', color='Who do you consider the most important person in your camp?', title='Who do you consider the most important person in your camp?')
fig.show()


# In[17]:


import plotly.express as px
# Grouping it by sex and question
plot_data = df.groupby(['Sex', 'Do you have enough good friends in the camp?'], as_index=False).Row_ID.sum()
fig = px.bar(plot_data, x='Sex', y='Row_ID', color='Do you have enough good friends in the camp?', title='Do you have enough good friends in the camp?')
fig.show()


# In[18]:


ax = df['Do you believe Rohingya and Bangladeshis can be good friends?'].value_counts().plot.barh(figsize=(10, 4))
ax.set_title('Do you believe Rohingya and Bangladeshis can be good friends?', size=18)
ax.set_ylabel('Do you believe Rohingya and Bangladeshis can be good friends?', size=14)
ax.set_xlabel('Count', size=12)


# In[19]:


ax = df['How much do you trust the NGOs in the camp?'].value_counts().plot.barh(figsize=(10, 4), color='r')
ax.set_title('How much do you trust the NGOs in the camp?', size=18)
ax.set_ylabel('How much do you trust the NGOs in the camp?', size=14)
ax.set_xlabel('Count', size=12)


# In[20]:


ax = df['Do you often feel stressed or overwhelmed by your situation?'].value_counts().plot.barh(figsize=(10, 4), color='g')
ax.set_title('Do you often feel stressed or overwhelmed by your situation?', size=18)
ax.set_ylabel('Do you often feel stressed or overwhelmed by your situation?', size=14)
ax.set_xlabel('Count', size=12)


# In[21]:


ax = df['Do you think that sexual harassment in the camp happens often?'].value_counts().plot.barh(figsize=(10, 4), color='orange')
ax.set_title('Do you think that sexual harassment in the camp happens often?', size=18)
ax.set_ylabel('Do you think that sexual harassment in the camp happens often?', size=14)
ax.set_xlabel('Count', size=12)


# In[22]:


ax = df['Where does sexual harassment happen the most?'].value_counts().plot.barh(figsize=(10, 4), color='purple')
ax.set_title('Where does sexual harassment happen the most?', size=18)
ax.set_ylabel('Where does sexual harassment happen the most?', size=14)
ax.set_xlabel('Count', size=12)


# In[23]:


df["How do you feel about the future?"].value_counts()


# In[24]:


import plotly.express as px
# Grouping it by sex and question
plot_data = df.groupby(['Sex', 'How do you feel about the future?'], as_index=False).Row_ID.sum()
fig = px.bar(plot_data, x='Sex', y='Row_ID', color='How do you feel about the future?', title='How do you feel about the future?')
fig.show()

